LETTER:=letter
PANDOC_LETTER_OPTIONS=--template scrlttr2_template.tex -V documentclass="scrlttr2" -V papersize="A4" -V fontsize="11pt" -V mainfont="DejaVu Sans" -V monofont="Hack" -V mathfont="STIX Math" -V sansfont="Lato" -V colorlinks=true -V monofontoptions="Scale=0.8"
PANDOC_OPTIONS=--standalone -f markdown+smart --pdf-engine=xelatex

letter: $(LETTER).md
	pandoc $(PANDOC_OPTIONS) $(PANDOC_LETTER_OPTIONS) -s $(LETTER).md -o $(LETTER).pdf

